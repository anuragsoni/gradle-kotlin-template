import kotlin.test.Test
import kotlin.test.assertEquals

class TestGreeter {
    @Test
    fun testGreeter() {
        assertEquals(expected = "Hello, Demo", actual = greet("Demo"))
    }
}
