fun greet(name: String? = null): String = buildString {
    append("Hello, ")
    append(name ?: "Kotlin")
}
