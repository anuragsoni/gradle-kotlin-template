rootProject.name = "sandbox"

// Plugin to automatically download JDK versions configured via gradle's toolchain support.
plugins { id("org.gradle.toolchains.foojay-resolver-convention") version ("0.4.0") }

include(":library", ":bin")
