import com.diffplug.gradle.spotless.SpotlessExtension
import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask

plugins {
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.spotless)
    alias(libs.plugins.versions)
    alias(libs.plugins.version.catalog.update)
}

repositories { mavenCentral() }

kotlin { jvmToolchain(17) }

subprojects {
    apply(plugin = "org.jetbrains.kotlin.jvm")
    apply(plugin = "com.diffplug.spotless")
    repositories { mavenCentral() }

    kotlin { jvmToolchain(17) }

    tasks.withType<Test> { useJUnitPlatform() }

    configure<SpotlessExtension> {
        kotlin { ktfmt("0.46").kotlinlangStyle() }

        kotlinGradle { ktfmt("0.46").kotlinlangStyle() }
    }
}

configure<SpotlessExtension> {
    kotlin { ktfmt("0.46").kotlinlangStyle() }

    kotlinGradle { ktfmt("0.46").kotlinlangStyle() }
}

fun isUnstableRelease(version: String): Boolean {
    return version.contains("beta|rc|alpha".toRegex(RegexOption.IGNORE_CASE))
}

tasks.withType<DependencyUpdatesTask> {
    rejectVersionIf { isUnstableRelease(candidate.version) && !isUnstableRelease(currentVersion) }
}
