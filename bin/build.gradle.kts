import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.shadow)
    application
}

dependencies { implementation(project(":library")) }

tasks.withType<ShadowJar> { minimize() }

application { mainClass.set("MainKt") }
